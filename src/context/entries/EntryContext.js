import { createContext, useReducer } from "react";
import EntryReducter from "./EntryReducer";

const EntryContext = createContext();

export const EntryProider = ({children}) => {
    const initalState = {
        entries: [],
        totalExpense: 0,
        totalIncome: 0,
        loading: true,
    }
    const [state, dispatch] = useReducer(EntryReducter, initalState);
    return <EntryContext.Provider value={{
        ...state,
        dispatch
    }}>
        {children}
    </EntryContext.Provider>
}

export default EntryContext;