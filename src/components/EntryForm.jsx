import React, { useContext, useState } from 'react'
import { toast } from 'react-toastify'
import { addEntry, getEntries } from '../context/entries/EntryAction';
import EntryContext from '../context/entries/EntryContext';

function EntryForm() {
  /** set states for form */
  const [title, setTitle] = useState("");
  const [amount, setAmount] = useState(0);
  const [item_type, setItemType] = useState("income");

  /** using entry context dispatch */
  const { dispatch } = useContext(EntryContext);


  /** handle form submit function */
  const handleSubmit = async(e) => {
    e.preventDefault();
    dispatch({ type: 'SET_LOADING' });
    if (title === "" || amount === "") {
      toast.error("You must provide the title and amount");
      return false;
    }
    await addEntry(title, amount, item_type);
    toast.success("Entry added");
    setTitle("");
    setAmount("");
    setItemType("income");

    
    const allEntries = await getEntries();
    dispatch({ type: 'GET_ENTRIES', payload: allEntries });
    
  }
  return (
    <form onSubmit={handleSubmit}>
      <table className="table table-bordered">
        <tbody>
          <tr>
            <td><input type="text" className='form-control' id="title" name="title" value={title} placeholder="Title" onChange={(e) => setTitle(e.target.value)} /></td>
          </tr>
          <tr>
            <td><input type="number" step=".1" min="0" className='form-control' id="amount" name="amount" value={amount} placeholder="Amount" onChange={(e) => setAmount(e.target.value)} /></td>
          </tr>
          <tr>
            <td>
              <select name="item_type" className='form-control' value={item_type} id="item_type" onChange={(e) => setItemType(e.target.value)}>
                <option value="income">Income</option>
                <option value="expense">Expense</option>
              </select>
            </td>
          </tr>
        </tbody>
      </table>
      <button className='btn btn-primary' type="submit">Send</button>
    </form>
  )
}

export default EntryForm