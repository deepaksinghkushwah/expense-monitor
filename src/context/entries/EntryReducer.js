const EntryReducter = (state, action) => {
    let expense = 0;
    let income = 0;
    switch(action.type){
        case 'GET_ENTRIES':
            expense = setTotal('expense', action.payload);
            income = setTotal('income', action.payload)
            return {                
                entries: action.payload,
                loading: false,
                totalExpense: expense,
                totalIncome: income
            }
       
        case 'SET_LOADING':
            return {
                loading: true
            }
        default:
            return state;
    }
}

function setTotal(type, entries){
    let total = 0.00;
    console.log(entries);
    entries.map((item) => {
        if(item.item_type === type){
            total += parseFloat(item.amount);
        }
    })
    return total;    

}

export default EntryReducter;