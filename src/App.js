import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import EntryForm from "./components/EntryForm";
import { EntryProider } from "./context/entries/EntryContext";
import EntryList from "./components/EntryList";
import Modal from "react-modal";
import { useState } from "react";

function App() {
  // set app element to root
  Modal.setAppElement("#root");

  // set state for open and close modal
  const [modelIsOpen, setModelIsOpen] = useState(false);

  // open modal function
  const openModal = () => {
    setModelIsOpen(true);
  };

  // close modal function
  const closeModal = () => {
    setModelIsOpen(false);
  };

  return (
    <div className="App">
      {/** Entry provider for entry context */}
      <EntryProider>
        {/** Modal config start */}
        <Modal
          isOpen={modelIsOpen}
          onRequestClose={closeModal}
          className="customModal mt-5 p-2"
        >
          <button
            onClick={closeModal}
            className="btn btn-sm btn-danger float-end"
          >
            close
          </button>
          <EntryForm />
        </Modal>
        <h1 className="mt-3 ms-3">
          Expense Monitor
          <span className="float-end me-3">
            <button
              type="button"
              className="btn btn-primary btn-sm"
              onClick={openModal}
            >
              Add Entry
            </button>
          </span>
        </h1>

        {/** Expense entries module */}
        <EntryList />

        {/** Toast container to show toast notifications */}
        <ToastContainer />
      </EntryProider>
    </div>
  );
}

export default App;
