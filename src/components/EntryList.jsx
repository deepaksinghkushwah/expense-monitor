import React, { useEffect } from 'react'
import { useContext } from 'react';
import { getEntries, removeEntry } from '../context/entries/EntryAction';
import EntryContext from "../context/entries/EntryContext";
import { FaTrash } from 'react-icons/fa';
import { toast } from 'react-toastify';
import moment from "moment";
function EntryList() {
    /** use entry context to get fields */
    const { entries, dispatch, totalIncome, totalExpense, loading } = useContext(EntryContext);

    useEffect(() => {
        dispatch({ type: 'SET_LOADING' });
        const fetchEntries = async () => {
            const r = await getEntries();
            dispatch({ type: 'GET_ENTRIES', payload: r });

        }
        fetchEntries();

    }, [dispatch]);

    /** handle delete event */
    const handleDelete = async (id) => {
        if (window.confirm("Are you sure want to remove this entry?")) {
            dispatch({ type: 'SET_LOADING' });
            await removeEntry(id);
            toast.success("Item deleted");
            const r = await getEntries();
            dispatch({ type: 'GET_ENTRIES', payload: r });
        }


    }

    if (loading) {
        return "Loading...";
    }
    return (
        <>
            {/** return entries if entries have rows */}
            {entries && entries.length > 0 ? (
                <table className='table table-hover table-small'>
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {entries.map((item) => (
                            <tr key={item.id} className={item.item_type === 'expense' ? "table-danger" : "table-primary"} title={item.item_type}>
                                <td>
                                    {item.title}
                                </td>
                                <td>
                                    ${item.amount}
                                </td>
                                <td>{moment(item.date).format("MMMM Do YYYY, h:mm:ss a")}</td>
                                <td>
                                    <span className='float-end pe-3' onClick={() => handleDelete(item.id)}><FaTrash /></span>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                    <tfoot className='table-secondary'>
                        <tr>
                            <th>Total Income</th>
                            <th>${totalIncome}</th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>Total Expense</th>
                            <th>${totalExpense}</th>
                            <th></th>
                            <th></th>
                        </tr>

                    </tfoot>
                </table>

            ) : 'No entries found'}
        </>

    )
}

export default EntryList