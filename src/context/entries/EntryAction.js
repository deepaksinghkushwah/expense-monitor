import axios from "axios"
import moment from "moment";
const http = axios.create({
    baseURL: 'http://localhost:5000'
}); 

export const addEntry = async(title, amount, item_type) => {
    const date = moment().format('LLLL');    
    const params = new URLSearchParams({title, amount, item_type, date });
    const response = await http.post('/entries',params);
    const data = await response.data;
    return data;
}

export const getEntries = async() => {
    const r = await http.get('/entries');
    const data = await r.data;
    return data;
}

export const removeEntry = async(id) => {
    const response = await http.delete(`/entries/${id}`)
    const data = await response.data;
    return data;
}